﻿using Newtonsoft.Json.Linq;
using RestCountries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RestCountries
{
    public class CountrySearch
    {
        public static async Task<Country> Searching(string countryName)
        {
            Country country = null;
            using (var client = new HttpClient())
            {
                try
                {
                    var responseString = await client.GetStringAsync($"https://restcountries.eu/rest/v2/name/{countryName}");
                    responseString = responseString.Remove(responseString.Length - 1);
                    responseString = responseString.Remove(0, 1);

                    JObject json = JObject.Parse(responseString);
                    if(json != null)
                    {
                        country = new Country
                        {
                            Name = json.GetValue("name").ToString(),
                            Code = json.GetValue("alpha3Code").ToString(),
                            Capital = new City { Name = json.GetValue("capital").ToString() },
                            Area = Double.Parse(json.GetValue("area").ToString()),
                            Population = int.Parse(json.GetValue("population").ToString()),
                            Region = new Region { Name = json.GetValue("region").ToString() }
                    };
                    }
                }
                catch
                {
                    
                }
            }
            return country;
        }
    }
}
