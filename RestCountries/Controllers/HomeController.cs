﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestCountries.Data;
using RestCountries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace RestCountries.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext context;
        private IRepository repository;
        public HomeController(AppDbContext ctx, IRepository repo)
        {
            context = ctx;
            repository = repo;
        }

        public IActionResult Index() => View(repository.GetAllCountries());
        [HttpPost]
        public async Task<IActionResult> FindCountry(string countryName)
        {
            Country country = await CountrySearch.Searching(countryName);

            return View("SearchResult", country);
        }
        [HttpPost]
        public ActionResult SaveCountry(Country country)
        {
            repository.AddCountry(country);
            return View("Index", repository.GetAllCountries());
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Country>>> Get() => await context.Countries.ToListAsync();
    }
}
