﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestCountries.Models
{
    public class SeedData
    {
        public static void Seed(IApplicationBuilder app)
        {
            var scope = app.ApplicationServices.CreateScope();
            AppDbContext context = scope.ServiceProvider.GetRequiredService<AppDbContext>();
            //context.Database.Migrate();
            if(!context.Countries.Any())
            {
                City c1 = new City { Name = "Kiew" };
                City c2 = new City { Name = "Kabul" };

                context.Cities.Add(c1);
                context.Cities.Add(c2);

                Region r1 = new Region { Name = "Europe" };
                Region r2 = new Region { Name = "Asia" };

                context.Regions.Add(r1);
                context.Regions.Add(r2);

                Country co1 = new Country
                    {
                        Name = "Ukraine",
                        Code = "UKR",
                        Capital = c1,
                        Area = 603700.0,
                        Population = 42692393,
                        Region = r1
                    };
                Country co2 = new Country
                {
                    Name = "Afghanistan",
                    Code = "AFG",
                    Capital = c2,
                    Area = 652230.0,
                    Population = 27657145,
                    Region = r2
                };

                context.Countries.Add(co1);
                context.Countries.Add(co2);

                context.SaveChanges();
            }
        }
    }
}
