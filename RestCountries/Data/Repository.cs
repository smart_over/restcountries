﻿using Microsoft.EntityFrameworkCore;
using RestCountries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestCountries.Data
{
    public class Repository : IRepository
    {
        private AppDbContext context;
        public Repository(AppDbContext ctx) => context = ctx;

        public List<City> GetAllCities() => context.Cities.ToList();

        public List<Country> GetAllCountries() => context.Countries.Include(r => r.Region).Include(c => c.Capital).OrderBy(c => c.Name).ToList();

        public List<Region> GetAllRegions() => context.Regions.ToList();
        public void AddCountry(Country country)
        {
            country.Capital = GetCityFromDb(country.Capital);
            country.Region = GetRegionFromDb(country.Region);

            // Проверяем есть ли в базе страна с соответствующим кодом
            Country dbEntry = context.Countries.FirstOrDefault(c => c.Code == country.Code);
            // Если есть - обновляем ее значения
            if(dbEntry != null)
            {
                dbEntry.Name = country.Name;
                dbEntry.Area = country.Area;
                dbEntry.Capital = country.Capital;
                dbEntry.Population = country.Population;
                dbEntry.Region = country.Region;
            } // Иначе - добавляем
            else
            {
                context.Countries.Add(country);
            }

            context.SaveChanges();
        }
        public City GetCityFromDb(City city)
        {
            // Проверяем есть ли такой город в бд
            City dbEntry = context.Cities.FirstOrDefault(c => c.Name.ToUpper() == city.Name.ToUpper());
            // Если нет - добавляем
            if(dbEntry == null)
            {
                dbEntry = city;
                context.Cities.Add(dbEntry);
            }

            return dbEntry;
        }
        public Region GetRegionFromDb(Region region)
        {
            // Првоеряем есть такой регион в бд
            Region dbEntry = context.Regions.FirstOrDefault(r => r.Name.ToUpper() == region.Name.ToUpper());
            // Если нет - добавляем
            if (dbEntry == null)
            {
                dbEntry = region;
                context.Regions.Add(dbEntry);
            }
            return dbEntry;
        }
    }
}
