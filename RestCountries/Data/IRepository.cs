﻿using RestCountries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RestCountries.Data
{
    public interface IRepository
    {
        List<Country> GetAllCountries();
        List<Region> GetAllRegions();
        List<City> GetAllCities();
        void AddCountry(Country country);
        Region GetRegionFromDb(Region region);
        City GetCityFromDb(City city);
    }
}
